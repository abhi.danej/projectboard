This is a working project with all files from React and Spring Boot.

React files are placed in src/resources/static.

I used Spring Tool Suite v4 to develop and run the app. 
To run the app, you must import the project in your env and use ./mvnw clean install or ./mvnw spring-boot:run